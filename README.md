# rpm-vault-1.0.3-64-bit

Installs Hashi Vault 1.0.3 binaries and auto complete for vault.

For help, use the `vault -h` command after installation. To use the auto complete feature, reload your shell, this can be down by entering the `exec $SHELL` command. 